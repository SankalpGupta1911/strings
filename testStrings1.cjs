const strToNum = require("./strings1.cjs");

console.log(strToNum("abcd-.32xyz"));
console.log(strToNum("abcd86.32xyz"));
console.log(strToNum("abcd.32xyz"));
console.log(strToNum("abcd632xyz"));
console.log(strToNum("abcd-786.32xyz"));
console.log(strToNum("abcd..32xyz"));
console.log(strToNum("abcd.-32xyz"));