function str1(str){
    let result = str.replace(/[^.0-9-]/g,"");
    result = Number(result);
    if (Number.isNaN(result)){
        return 0;
    }
    return result;
}

module.exports = str1;
