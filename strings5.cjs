module.exports = function joinstrings(arr){
    if(!Array.isArray(arr)|| arr.length<1) return "";
    return arr.join(" ");
}

