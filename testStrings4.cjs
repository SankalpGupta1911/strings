const fullName = require("./strings4.cjs");

console.log(fullName({1:"sankALp", 2: "kumar", 3: "gupta", 4: "45"}));
console.log(fullName({"first_name": "JoHN", "last_name": "SMith"}));
console.log(fullName({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}));