const ipaddress = require("./strings2.cjs");

console.log(ipaddress("111.139.161.143"));
console.log(ipaddress("11S1.139.161.143"));
console.log(ipaddress("111.23.139.161.143"));
console.log(ipaddress("111.13+*9.161.143"));
console.log(ipaddress("111.132Shdi9.161.143"));