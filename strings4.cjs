module.exports = function fullName(obj){
    if(obj==undefined || typeof(obj) != "object" || obj.length<1) return [];
    let arr = Object.values(obj);
    const str = arr.join(" ");
    let name = str.toLowerCase();
    name = name.split(" ");
    for(let i = 0; i<name.length ; i++){
        if (isNaN(Number(name[i]))){
            name[i] = name[i].charAt(0).toUpperCase() + name[i].substring(1,name[i].length);
        }
        else{
            name[i]="";
        }
        
    }
    return name.join(" ");
}